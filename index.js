const pug = require('pug')
const express = require('express')
const app = express()


// plugins
if (process.env.NODE_ENV == 'production') {
	app.use(require('compression')())
	app.use(require('express-http-to-https').redirectToHTTPS())
}


// middlewares
app.use(require('./middlewares/404-middleware.js'))
app.use(require('./middlewares/pug-middleware.js'))
app.use(require('./middlewares/sass-middleware.js'))
app.use(require('./middlewares/terser-middleware.js'))


// static fiels
app.use(express.static('public'))


// '/' route
app.get('/', (req, res) =>
	res.send(pug.renderFile('public/index.pug'))
)


// listen
const PORT = process.env.PORT || 8000
app.listen(PORT, () => console.log(`Listening on http://localhost:${PORT}/`))