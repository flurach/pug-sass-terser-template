# Pug, Sass, Terser Template
This is a template using `pug`, `dart-sass` and `terser` with `node`/`express`
and `yarn` as the package manager.

The files with extensions `.pug`, `.sass` and `.js` inside `public/` get
compiled and compressed on the fly. So you can craft beautiful and performant
front-ends without worrying about back-end structure. But you can, indeed
write good back-end using `express` by integrating it with a database.

## How to start developing
All you need is

- yarn
- node

Firstly, install dependencies with `yarn`. Then, you can do:
``` sh
$ yarn dev
```
Which starts a `nodemon` session.

Alternatively, you can do `yarn start` to use `node` instead of `nodemon`.


## Used stack

- express
- pug
- dart-sass
- terser
- nodemon

With `node` and `yarn` as the package manager.