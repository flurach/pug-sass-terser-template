const fs = require('fs')
const path = require('path')


module.exports = (req, res, next) => {
	const default = path.resolve('public/404.pug')
	const file = path.resolve('public') + req.originalUrl

	if (fs.existsSync(file) == false) {
		if (fs.existsSync(file))
			return res.sendFile(default)

		return res.send('404')
	}

	next()
}